using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Tobii.Gaming;

/// <summary>
/// Activa el menu de opciones durante la carrera
/// </summary>
public class MenuOptionsRace : MonoBehaviour
{
    public GameObject UnPauseButton, RaceAGain, MainMenu, TobiiPanels;

    // Grafico que rellena el reloj circular 
    public Image CircularBar;
    
    // Camara para situar el reloj circular en la posicion correcta
    public Camera cam;

    // Valor actual del reloj circular
    float _currentValue;
    
    // Velocidad de complecion del reloj circular
    public float speed;
    
    // Objeto sobre el que esta situado el reconocimiento circular
    GameObject _focusedObject;
    
    // Start is called before the first frame update
    void Start()
    {
        // Se habilita el reloj circular
        CircularBar.fillAmount = 0;
        _currentValue = 0;
        CircularBar.gameObject.SetActive(true);
        speed = 40;
    }

    // Update is called once per frame

    void Update()
    {
        
        // Obtenemos el objeto sobre el que se encuentra actualmente
        // enfocado el reconocimiento ocular
        _focusedObject = TobiiAPI.GetFocusedObject();

        // Si no hay objeto seleccionado, el reloj se reinicia
        if (_focusedObject == null)
        {
            _currentValue = 0;
            CircularBar.fillAmount = 0;
        }

        else
        {
            if (string.Equals(_focusedObject.name, this.name) && _currentValue < 100)
            {
                // Si el objeto sobre el que se encuentra el reconocimiento ocular
                // es el mismo que en el paso de Update anterior, incrementamos
                // el reloj segun la velocidad de llenado siempre que este a menos
                // del 100% del total
                _currentValue += speed * Time.unscaledDeltaTime;
                CircularBar.transform.position = cam.WorldToScreenPoint(_focusedObject.transform.position);
            }
            else
                _currentValue = 0;
        }


        // Actualizamos el grafico con el reloj circular
        CircularBar.fillAmount = _currentValue / 100;

        // Si el reloj circular esta al 100%, se reproduce el sonido de seleccion 
        // y fundimos a negro, dando paso a la escena apropiada en funcion
        // de la opcion que haya sido elegida
        
        // Es un control temporal para las pruebas con teclado
        if ((CircularBar.fillAmount >= 1 && CircularBar.gameObject.activeSelf) || Input.GetKeyDown(KeyCode.Space))
        // if (CircularBar.fillAmount >= 1 && CircularBar.gameObject.activeSelf)
        {
            _currentValue = 0;
            CircularBar.fillAmount = 0;

            Physics.autoSyncTransforms = true;

            Time.timeScale = 0;

            // Se desactivan los paneles de cambio de carril
            TobiiPanels.gameObject.SetActive(false);
            
            // Se muestra el boton para salir del Menu de pausa
            UnPauseButton.gameObject.SetActive(true);

            // Se muestra el boton para reiniciar la carrera
            RaceAGain.gameObject.SetActive(true);

            // Se muestra el boton para volver al menu principal
            MainMenu.gameObject.SetActive(true);

            gameObject.SetActive(false);

        }

    }

}
