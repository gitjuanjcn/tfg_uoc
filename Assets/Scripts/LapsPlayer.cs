using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script contar las vueltas que va completando el jugador y desencadenar
/// efectos adicionales al final de la carrera que puedan ser especificos
/// del modo de juego contra vehiculos rivales
/// </summary>
public class LapsPlayer : MonoBehaviour
{
    // Contador de vueltas para el jugador
    public int LapCounter;

    // Checkpoints a lo largo del circuito
    public GameObject StartLine, BehindGoal, CheckMiddle, FinishRace;

    // Muestra las vueltas completadas
    public GameObject PlayerLapsIcon;

    // Mensaje al final de la carrera que cambia dependiendo del vehiculo ganador
    public GameObject RaceMessage;

    // Checkpoint para los vehiculos rivales
    public GameObject CheckRivals;

    void Awake()
    {
        LapCounter = 0;
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        // Si el jugador toca el objeto, incrementamos 
        // en 1 el contador de vueltas
        if (other.tag == "ColBody")
        {
            LapCounter++;
            PlayerLapsIcon.GetComponent<TMPro.TextMeshProUGUI>().text = "Vueltas: " + LapCounter + "/3";
            // Si ha dado menos de tres vueltas, 
            // la carrera continua
            if (LapCounter < 3)
            {
                StartLine.SetActive(true);
                BehindGoal.SetActive(true);
                CheckMiddle.SetActive(false);

            }
            // Si se han completado tres vueltas, se
            // desactiva el checkpoint para vehiculos rivales
            // y se activa el objeto que pone fin a la carrera
            else
            {
                CheckRivals.SetActive(false);

                StartLine.SetActive(false);
                BehindGoal.SetActive(false);
                CheckMiddle.SetActive(false);

                RaceMessage.GetComponent<TMPro.TextMeshProUGUI>().text = "¡ENHORABUENA!";

                FinishRace.SetActive(true);
            }

            gameObject.SetActive(false);
        }
    }
}
