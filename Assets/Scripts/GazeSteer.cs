using System.Collections;
using System.Collections.Generic;
using Tobii.Gaming;
using UnityEngine;

[RequireComponent(typeof(GazeAware))]

/// <summary>
/// Script que permite al jugador elegir el carril central
/// </summary>
public class GazeSteer : MonoBehaviour
{
    // Objeto que representa al vehiculo del jugador
    //public GameObject Player;

    // Componente Tobii necesario para activar 
    // el reconocimiento ocular
    private GazeAware _gazeAwareComponent;

    public float axisValue;

    void Start()
    {
        // Se activa el reconomiento visual
        _gazeAwareComponent = GetComponent<GazeAware>();
    }
}
