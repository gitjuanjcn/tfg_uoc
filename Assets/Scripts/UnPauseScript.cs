using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnPauseScript : MonoBehaviour
{
    public GameObject RestartRace, MainMenu, MenuOption, TobiiPanels;

    public void continueRace()
    {
        RestartRace.SetActive(false);
        MainMenu.SetActive(false);
        MenuOption.SetActive(true);
        TobiiPanels.SetActive(true);

        Time.timeScale = 1;
        
        gameObject.SetActive(false);
    }

}
