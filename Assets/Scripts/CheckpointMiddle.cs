using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointMiddle : MonoBehaviour
{
    // Checkpoints a lo largo del circuito
    public GameObject StartLine, GoalLine, BehindGoal;

    private void OnTriggerEnter(UnityEngine.Collider other)
    {

        StartLine.SetActive(true);
        GoalLine.SetActive(true);
        BehindGoal.SetActive(false);

        gameObject.SetActive(false);

    }
}
