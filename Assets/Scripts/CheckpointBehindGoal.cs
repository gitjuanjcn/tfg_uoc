using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointBehindGoal : MonoBehaviour
{
    public GameObject StartLine, GoalLine, CheckMiddle;


    private void OnTriggerEnter(UnityEngine.Collider other)
    {

        StartLine.SetActive(true);
        GoalLine.SetActive(false);
        CheckMiddle.SetActive(false);

        gameObject.SetActive(false);

    }
}
