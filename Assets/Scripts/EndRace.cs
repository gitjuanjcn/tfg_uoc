using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script para marcar el final de la carrera, activado cuando el jugador colisiona
/// contra un objeto invisible situado en la linea de meta. Desactiva los controles,
/// muestra la puntuacion final y ofrece un boton para reiniciar la carrera.
/// </summary>
public class EndRace : MonoBehaviour
{
    // Cartel con el resumen de la carrera
    public Image ResultsBackground;
    // Obketos para el vehiculo del jugador, botones de opciones tras la
    // carrera, objetos y paneles de control de cambio de carril
    public GameObject ModelCar, RaceAGain, MainMenu, MenuOption, TobiiPanels, GroupWayPoints;

    // Barra de progreso para menus
    public Image CircularBar;

    // Clips de musica de fondo de la carrera y de fin de carrera
    public AudioSource BackMusic, EndMusic;

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.gameObject.tag == "ColBody")
        {
            // Se detiene la musica de fondo y comienza la de fin de carrera
            BackMusic.Stop();
            EndMusic.Play();

            // Se desactivan los paneles de cambio de carril
            TobiiPanels.gameObject.SetActive(false);

            GroupWayPoints.SetActive(false);

            // Se muestra el panel de resultados
            ResultsBackground.gameObject.SetActive(true);

            // Se oculta el boton para abrir el menu de opciones
            MenuOption.gameObject.SetActive(false);

            // Se muestra el boton para reiniciar la carrera
            RaceAGain.gameObject.SetActive(true);
            
            // Se muestra el boton para volver al menu principal
            MainMenu.gameObject.SetActive(true);

            // Se habilita el reloj circular
            CircularBar.fillAmount = 0;
            CircularBar.gameObject.SetActive(true);
            
            Physics.autoSyncTransforms = true;

            Time.timeScale = 0;

            //Se desactiva el objeto utilizado para activar el fin de la carrera
            gameObject.SetActive(false);

        }
    }


}
