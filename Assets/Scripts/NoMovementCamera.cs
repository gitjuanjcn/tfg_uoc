using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para estabilizar el movimiento de la camara, de forma que
/// la imagen no tiemble o vibre con el desplazaminto del jugador
/// </summary>
public class NoMovementCamera : MonoBehaviour
{
    // Representa al jugador
    public GameObject Player;

    // Las vibraciones en la imagen se miniminzan en funcion del jugador
    public float PlayerX;
    public float PlayerY;
    public float PlayerZ;

    void Update()
    {
        // En cada paso Update tomamos los angulos de orientacion del jugador
        PlayerX = Player.transform.eulerAngles.x;
        PlayerY = Player.transform.eulerAngles.y;
        PlayerZ = Player.transform.eulerAngles.z;

        // Los angulos corregidos se obtienen de neutralizar las rotaciones 
        // X y Z del jugador, que son los que pueden hacer voltear el movimiento 
        // de la camara hacia adelante o hacia los lados.
        transform.eulerAngles = new Vector3(PlayerX - PlayerX, PlayerY, PlayerZ - PlayerZ);
    }
}