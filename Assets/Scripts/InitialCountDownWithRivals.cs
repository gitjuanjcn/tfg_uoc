using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script para la cuenta atras inicial, de modo que la carrera no de comienzo
/// de inmediato y resulte mas inmersivo. Los controles del vehiculo y el
/// desplazamiento automatico estan desactivados al principio, y se habilitan
/// una vez la cuenta atras termina.
/// </summary>
public class InitialCountDownWithRivals : MonoBehaviour
{
    // Letrero con los mensajes a mostrar durante al cuenta atras
    public GameObject CountDownText;

    // Sonidos de cuenta atras e inicio de carrera
    public AudioSource BeepClock, StartRace;

    // Paneles de control de cambio de carril
    public GameObject TobiiPanels;

    // Vehiculo que representa al jugador
    public GameObject PlayerCar;

    // Objeto que contiene los veh�culos rivales
    public GameObject Rivals;

    // Objeto para el menu de opciones durante la carrera
    public GameObject MenuOption;

    // Musica de fondo
    public AudioSource BackMusic;

    void Start()
    {
        // Se da inicio a la corrutina de cuenta atras, 
        // que requiere varios retardos controlados
        StartCoroutine(CountStart());
    }

    IEnumerator CountStart()
    {
        // La cuenta atras consiste en varias activaciones y desactivaciones del
        // letrero en el que se imprime el mensaje para el jugador, cambiandolo
        // cada vez. Se acompa�an de un sonido que cambia con el ultimo mensaje,
        // incorporando un peque�o retraso para controlar el tiempo apropiadamente.
        yield return new WaitForSeconds(0.5f);
        CountDownText.GetComponent<TMPro.TextMeshProUGUI>().text = "Empezamos en 3";

        // Sonido de la cuenta atras
        BeepClock.Play();

        // Activacion y desactivacion del mensaje
        CountDownText.SetActive(true);
        yield return new WaitForSeconds(1);
        CountDownText.SetActive(false);

        // Cambio del contenido del mensaje
        CountDownText.GetComponent<TMPro.TextMeshProUGUI>().text = "2";

        BeepClock.Play();

        CountDownText.SetActive(true);
        yield return new WaitForSeconds(1);
        CountDownText.SetActive(false);
        CountDownText.GetComponent<TMPro.TextMeshProUGUI>().text = "1";

        BeepClock.Play();

        CountDownText.SetActive(true);
        yield return new WaitForSeconds(1);
        CountDownText.SetActive(false);
        CountDownText.GetComponent<TMPro.TextMeshProUGUI>().text = "�VAMOS!";

        // Sonido del ultimo mensaje
        StartRace.Play();
        
        // Se activan los paneles que permiten el cambio de carril
        TobiiPanels.SetActive(true);

        // Se activan los script de desplazamiento automatico y 
        // de reconocimiento de carril actual
        PlayerCar.GetComponent<AIPlayerCar>().enabled = true;

        foreach (Transform RivalCar in Rivals.transform)
            RivalCar.GetChild(0).GetComponent<AIRivalCar>().enabled = true;

        CountDownText.SetActive(true);
        yield return new WaitForSeconds(1);
        
        // Comienza la musica de fondo
        BackMusic.Play();

        // Se activa el menu de opciones durante la carrera
        MenuOption.SetActive(true);

        // Se desactiva la cuenta atras
        CountDownText.SetActive(false);

    }

}
