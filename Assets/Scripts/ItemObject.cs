using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script que define los atributos basicos de 
/// los objetos y les hace rotar sobre si mismos
/// </summary>
public class ItemObject : MonoBehaviour
{
    // Atributos para el valor en puntos del
    // objeto, asi como los efectos (sonido
    // y particulas) que se desencadenaran
    // al recolectarlo
    
    public short score;
    public string soundPick;
    public string particlesPick;

    private void Update()
    {
        // El objeto rotara sobre si mismo sobre el eje z.
        // Es un efecto puramente estetico y no afecta al
        // componente de colision a la hora de recolectarlo
        float zAxis = transform.rotation.z;
        
        transform.Rotate(new Vector3(0,0,zAxis + 45f) * Time.deltaTime);
    }
}

