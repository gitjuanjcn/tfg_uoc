﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Tobii.Gaming;


[RequireComponent(typeof(GazeAware))]

/// <summary>
/// Script para seleccionar las opciones del menu principal. Cuando
/// se situa el reconocimiento ocular sobre uno de los botones, se
/// activa un reloj circular que, una vez lleno, da por valida la
/// opcion seleccionada. Apartar el reconocimiento ocular o elegir
/// otro boton reinicia el reloj.
/// </summary>
public class GazeOptionsMain : MonoBehaviour
{
    // Nombre del objeto sobre el que se encuentra el reconocimiento ocular,
    // tanto en el paso de Update actual como en el anterior
    private string nameOption, nameOptionTemp;

    // Grafico que rellena el reloj circular 
    public Image CircularBar;
    
    // Camara para situar el reloj circular en la posicion correcta
    public Camera cam;

    // Valor actual del reloj circular
    float _currentValue;

    // Velocidad de complecion del reloj circular
    public float speed;

    // Objeto sobre el que esta situado el reconocimiento circular
    GameObject _focusedObject;
 
    // Sonido de confirmacion de seleccion
    public AudioSource SelectedSound;

    // Animacion para fundido a negro cuando se ha seleccionado
    // una opcion y hay un cambio de escena
    public Animator AnimatorFade;

    // Flag temporal para usar los controles por teclado.
    private bool tempKey = true;


    void Update()
    {
        // Controles de testeo con teclado
        if (Input.GetKeyDown(KeyCode.Return) && tempKey)
        {
            tempKey = false;
            CircularBar.gameObject.SetActive(false);
            SelectedSound.Play();
            AnimatorFade.SetTrigger("FadeOutTrigger");
            StartCoroutine(ChangeSceneAsync(2));
        }

        if (Input.GetKeyDown(KeyCode.Space) && tempKey)
        {
            tempKey = false;
            CircularBar.gameObject.SetActive(false);
            SelectedSound.Play();
            AnimatorFade.SetTrigger("FadeOutTrigger");
            StartCoroutine(ChangeSceneAsync(3));
        }

        if (Input.GetKeyDown(KeyCode.Escape) && tempKey)
        {
            tempKey = false;
            CircularBar.gameObject.SetActive(false);
            SelectedSound.Play();
            AnimatorFade.SetTrigger("FadeOutTrigger");
            StartCoroutine(ChangeSceneAsync(-1));
        }
        // Fin de controles de testeo



        // Obtenemos el objeto sobre el que se encuentra actualmente
        // enfocado el reconocimiento ocular
        _focusedObject = TobiiAPI.GetFocusedObject();
        
        // Si no hay objeto seleccionado, el reloj se reinicia
        if (_focusedObject == null)
        {
            _currentValue = 0;
        }
        else
        {
            // Si el objeto sobre el que se encuentra el reconocimiento ocular
            // es el mismo que en el paso de Update anterior, incrementamos
            // el reloj segun la velocidad de llenado siempre que este a menos
            // del 100% del total
            nameOption = _focusedObject.name;

            if (string.Equals(nameOption, nameOptionTemp) && _currentValue < 100)
                _currentValue += speed * Time.deltaTime;
            else
                _currentValue = 0;
        }

        // Actualizamos el nombre del objeto focalizado por el reconocimiento ocular
        nameOptionTemp = nameOption;

        // Actualizamos el grafico con el reloj circular
        CircularBar.fillAmount = _currentValue / 100;

        // Si el reloj circular esta al 100%, se reproduce el sonido de seleccion 
        // y fundimos a negro, dando paso a la escena apropiada en funcion
        // de la opcion que haya sido elegida
        if (CircularBar.fillAmount >= 1 && CircularBar.gameObject.activeSelf)
        {
            CircularBar.gameObject.SetActive(false);
            SelectedSound.Play();
            AnimatorFade.SetTrigger("FadeOutTrigger");

            // Se llama a la corrutina que controla el cambio de escena
            switch (nameOption)
            {
                case "PlayPoints":
                    StartCoroutine(ChangeSceneAsync(2));
                    break;

                case "PlayRivals":
                    StartCoroutine(ChangeSceneAsync(3));

                    break;

                case "ExitGame":
                    StartCoroutine(ChangeSceneAsync(-1));
                    break;
            }
        }
    }

    /// <summary>
    /// Corrutina para el cambio de escena, que sera elegida
    /// en funcion del parametro pasado en la llamada
    /// </summary>
    /// <param name="scene">Escena a seleccionar, coincidente
    /// con el orden de construccion del proyecto.
    /// -1 -> Abandonar el juego
    /// 1 -> Pantalla del titulo y menu principal
    /// 2 -> Carrera con puntos
    /// 3 -> Carrera contra rivales</param>
    /// <returns></returns>
    private IEnumerator ChangeSceneAsync(int scene)
    {
        // El cambio de escena se realiza con dos segundos de retraso, para
        // evitar un salto abrupto y dar tiempo al fundido a negro
        yield return new WaitForSeconds(2);

        if (scene < 0)
            Application.Quit();
        else
            SceneManager.LoadScene(scene);

    }
}
