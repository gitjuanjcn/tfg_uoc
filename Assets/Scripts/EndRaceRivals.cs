using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script para marcar el final de la carrera, activado cuando el jugador colisiona
/// contra un objeto invisible situado en la linea de meta. Desactiva los controles,
/// muestra la puntuacion final y ofrece un boton para reiniciar la carrera.
/// </summary>
public class EndRaceRivals : MonoBehaviour
{
    // Cartel con el resumen de la carrera
    public Image ResultsBackground;
    // Obketos para el vehiculo del jugador, botones de opciones tras la
    // carrera, objetos y paneles de control de cambio de carril
    public GameObject ModelCar, RaceAGain, MainMenu, TobiiPanels;
    
    // Barra de progreso para menus
    public Image CircularBar;

    // Array que contiene los vehiculos rivales
    public GameObject Rivals;

    // Clips de musica de fondo de la carrera y de fin de carrera
    public AudioSource BackMusic, EndMusic;

    private void Start()
    {
        Physics.autoSyncTransforms = true;
        Time.timeScale = 0;

        // Se desactiva la IA de los vehiculos rivales
        // estableciendo antes su velocidad a 0
        foreach (Transform RivalCar in Rivals.transform)
        {
            RivalCar.GetChild(0).GetComponent<AIRivalCar>().Speed = 0;
            RivalCar.GetChild(0).GetComponent<AIRivalCar>().enabled = false;
        }

        // Se detiene la musica de fondo y comienza la de fin de carrera
        BackMusic.Stop();
        EndMusic.Play();

        // Se desactivan los paneles de cambio de carril
        TobiiPanels.gameObject.SetActive(false);
        
        // Se muestra el panel de resultados
        ResultsBackground.gameObject.SetActive(true);
                    
        // Se muestra el boton para reiniciar la carrera
        RaceAGain.gameObject.SetActive(true);
            
        // Se muestra el boton para volver al menu principal
        MainMenu.gameObject.SetActive(true);

        // Se habilita el reloj circular
        CircularBar.fillAmount = 0;
        CircularBar.gameObject.SetActive(true);
        
        //Se desactiva el objeto utilizado para activar el fin de la carrera
        gameObject.SetActive(false);
    }


}
