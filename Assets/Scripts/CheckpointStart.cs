using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointStart : MonoBehaviour
{
    public GameObject GoalLine, BehindGoal, CheckMiddle;


    private void OnTriggerEnter(UnityEngine.Collider other)
    {

        BehindGoal.SetActive(true);
        CheckMiddle.SetActive(true);
        GoalLine.SetActive(false);

        gameObject.SetActive(false);

    }
}
