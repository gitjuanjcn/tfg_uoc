using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Este script introduce un lapso cuando se pasa de
/// la Splash Screen a la pantalla de Titulo / Menu
/// Principal, para evitar que se salte parte de la
/// animacion
/// </summary>
public class SceneBeforeTitle : MonoBehaviour
{
    void Update()
    {
        // Calculamos el tiempo de la Splash Screen
        // y cargamos la pantalla de titulo
        if (Time.timeSinceLevelLoad > 1.5f)
        {
            SceneManager.LoadScene(1);
        }
    }
}
