using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script de seguimiento de la camara hacia el jugador 
/// </summary>
public class TrackPlayer : MonoBehaviour
{
    // Propiedad transform del objeto que representa al jugador
    public Transform TrackTarget;

    // Velocidad de giro
    private float _speed = 1f;

    // Distancia inicial entre el jugador y la camara
    private float _distance;

    private float _offset;

    private void Start()
    {
        _offset = Vector3.Distance(transform.position, TrackTarget.position);
    }

    void LateUpdate()
    {
        _distance = Vector3.Distance(transform.position, TrackTarget.position);

        // La camara sigue al jugador intentando mantener una posicion equidistante
        transform.position = Vector3.MoveTowards(transform.position, TrackTarget.transform.position, _distance - _offset);

        Vector3 targetDirection = TrackTarget.position - transform.position;

        float _singleStep = _speed * Time.deltaTime;

        // El vector de rotacion se gira hacia el jugador a la velocidad establecida
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, _singleStep, _offset);
        
        // Se actualiza el giro
        transform.rotation = Quaternion.LookRotation(newDirection);
    }
}
