using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointRivals : MonoBehaviour
{
    // Vehiculo del jugador
    public GameObject Player;
    
    // CheckPoints a lo largo del circuito
    public GameObject StartLine, BehindGoal, CheckMiddle, GoalLine, FinishRace;

    // Mensaje al final de la carrera que cambia dependiendo del vehiculo ganador
    public GameObject RaceMessage;

    // Si un vehiculo rival colisiona con este objeto
    // incrementamos el numero de vueltas que ha dado
    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        
        other.GetComponent<AIRivalCar>().LapsCount++;

        int laps = other.GetComponent<AIRivalCar>().LapsCount;

        other.GetComponent<AIRivalCar>().RivalLapsIcon.GetComponent<TMPro.TextMeshProUGUI>().text = "Vueltas: " + laps + "/3";

        // Si un vehiculo rival completa tres vueltas, se
        // da por finalizada la carrera y se desactiva
        // la IA del jugador ademas de establecer su
        // velocidad a 0
        if (other.GetComponent<AIRivalCar>().LapsCount >= 3)
        {
            StartLine.SetActive(false);
            BehindGoal.SetActive(false);
            CheckMiddle.SetActive(false);
            GoalLine.SetActive(false);

            Player.GetComponent<AIPlayerCar>().Speed = 0;
            Player.GetComponent<AIPlayerCar>().enabled = false;

            RaceMessage.GetComponent<TMPro.TextMeshProUGUI>().text = "IntÚntalo de nuevo...";

            FinishRace.SetActive(true);
        }

    }
}
