using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Script de interaccion con los objetos collecionables y gestion del mensaje
/// de fin de carrera en el que se muestran los resultados. El jugador va sumando
/// o restando puntos en funciona de los objetos positivos o negativos que recoja;
/// al alcanzar la meta se muestra la puntuacion final y la cuenta de cada uno de
/// los tipos de objetos recogidos
/// </summary>
public class ItemInteraction : MonoBehaviour
{
    // Textos para, respectivamente: el calculo instantaeo de la puntacion 
    // en caso de quiera mostrarse en tiempo real, los objetos positivos, 
    // los objetos negativos y la puntuacion final
    public TextMeshProUGUI ScoreCalculate, TextStars, TextTraps, FinalScore;

    // Valor en puntos del objeto con el que se colisione
    private short _score;

    // Contador de puntos acumulados por el jugador
    private short _tempScore;

    // Numero de objetos positivos recolectados
    private short _gotStars;

    // Numero de objetos negativos recolectados
    private short _gotTraps;

    // Reproduce un sonido en funciona del tipo
    // de objeto con el que se ha colisionado
    private AudioSource _playItemSound;

    private void Start()
    {
        // Se inicializan a 0 los puntos y contadores
        _tempScore = 0;
        _gotStars = 0;
        _gotTraps = 0;

        // Se inicializa el reproductor de sonidos
        _playItemSound = this.gameObject.AddComponent<AudioSource>();
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.gameObject.tag == "Item")
        {
            // Desactivamos el objeto con el que se ha colisionado (recolectado)
            other.gameObject.SetActive(false);

            // Se reproduce el tipo de sonido que figura en los atributos del objeto recolectado
            _playItemSound.clip = Resources.Load<AudioClip>(other.gameObject.GetComponent<ItemObject>().soundPick);

            Instantiate(Resources.Load<GameObject>(other.gameObject.GetComponent<ItemObject>().particlesPick), other.transform.position, Quaternion.identity);

            _playItemSound.Play();

            // Se toma la puntuacion del objeto recolectado
            _score = other.gameObject.GetComponent<ItemObject>().score;

            // Si suma puntos es un objeto positivos y si los resta es un objeto negativo,
            // actualizamos los contadores de objetos recolectados apropiadamente
            if (_score > 0)
                _gotStars++;
            else if (_score < 0)
                _gotTraps++;

            // A�adimos los puntos al contador de puntuacion total
            short.TryParse(ScoreCalculate.text, out _tempScore);
            _tempScore += _score;

            // La puntuacion total nunca puede ser inferior a 0
            if (_tempScore < 0)
                _tempScore = 0;
            
            // Actualizamos el mensaje de puntuacion en tiempo real
            ScoreCalculate.SetText(_tempScore.ToString());

            // Actualizamos los mensajes que apareceran cuando la carrera termine
            TextStars.SetText("Estrellas : " + _gotStars.ToString());
            TextTraps.SetText("Trampas  : " + _gotTraps.ToString());
            FinalScore.SetText("PUNTUACI�N TOTAL: " + _tempScore.ToString());
        }
    }
}
