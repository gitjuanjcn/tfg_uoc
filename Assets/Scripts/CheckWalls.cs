using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckWalls : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.GetContact(0).normal);
        Vector3 temp = Vector3.Cross(transform.up, collision.GetContact(0).normal);
                
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("WallsInner"))
            transform.rotation = Quaternion.LookRotation(temp);
        else if (collision.collider.gameObject.layer == LayerMask.NameToLayer("WallsOuter"))
            transform.rotation = Quaternion.LookRotation(-temp);

    }
}
