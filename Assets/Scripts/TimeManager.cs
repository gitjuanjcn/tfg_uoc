using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Script que controla el cronometro con el
/// tiempo transcurrido durante la carrera
/// </summary>
public class TimeManager : MonoBehaviour
{
    // Objetos para el panel del cronometro y los controles de carril
    public GameObject TimeCalculate, TobiiPanels;

    // Contador de tiempo
    private static float _timer;

    // Conversion del tiempo en Timespan, para darle el formato 
    // de cronometro adecuado
    private TimeSpan _timeDisplay;

    private void Start()
    {
        _timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        // Comenzaremos la cuenta del tiempo en cuanto se activen los
        // paneles de control de carril, dando comienzo a la carrera
        if (TobiiPanels.activeSelf)
        {
            _timer += Time.deltaTime;
            _timeDisplay = TimeSpan.FromSeconds(_timer);
        }
        // Usamos el tiempo de tipo TimeSpan para pasarlo a formato minutos:segundos:milisegundos
        TimeCalculate.GetComponent<TMPro.TextMeshProUGUI>().text = _timeDisplay.ToString(@"mm\:ss\:ff");
    }
}
