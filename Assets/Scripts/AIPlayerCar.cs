using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

/// <summary>
/// Script para controlar el desplazamiento automatico del jugador en funcion del
/// waypoint actualmente seguido, asi como la velocidad de desplazamiento gobernada
/// por las ruedas implementadas mediante WheelCollider.
/// </summary>
public class AIPlayerCar : MonoBehaviour
{
    // Rigidbody del propio jugador
    private Rigidbody _playerRigidBody;

    // Angulo de giro maximo que alcanzan las ruedas
    public float maxSteerAngle = 45f;

    // Collider de las ruedas frontales
    public WheelCollider[] wheelsFront = new WheelCollider[2];

    // Velocidad a la que se mueve el jugador
    public float Speed = 20f;

    GameObject _focusedObject;

    float steerWheels;

    public float axisValue = 0.0f;

    void Start()
    {
        // Inicializamos el Rigidbody
        _playerRigidBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // Velocidad del rigidbody
        float velocity = _playerRigidBody.velocity.magnitude;
        if (velocity < 10)
            velocity = 10;
        
        _focusedObject = TobiiAPI.GetFocusedObject();
        
        // Si no hay panel de control seleccionado, el vehiculo no gira
        if (_focusedObject == null)
            axisValue = 0.0f;
        else
        {
            string nameObject = _focusedObject.name;
            if (string.Equals(nameObject, "ControlRight") || string.Equals(nameObject, "ControlLeft"))
            {
                // El eje de giro corresponde al panel de control elegido 
                axisValue = _focusedObject.GetComponent<GazeSteer>().axisValue;
            }
            else
                axisValue = 0.0f;
        }


        // Control con teclado para pruebas para testeo
        axisValue = Input.GetAxis("Horizontal");

        // Orientamos las ruedas
        // El giro es de 40� orientados hseg�n el panel de control seleccionado,
        // decrementado seg�n la velocidad para evitar inestabilidad
        steerWheels = Mathf.LerpAngle(0.0f, axisValue * 40f, 1f) / velocity;
        
        wheelsFront[0].steerAngle = steerWheels;
        wheelsFront[1].steerAngle = steerWheels;
        
        // Se asigna velocidad y poder de rotacion (torque) a las ruedas
        wheelsFront[0].motorTorque = Speed * 500 * Time.deltaTime;
        wheelsFront[1].motorTorque = Speed * 500 * Time.deltaTime;
    }

}
