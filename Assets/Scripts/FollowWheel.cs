using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para que las entidades 3D que representan a las ruedas imiten el giro
/// de los WheelCollider que realmente gobiernan el movimiento del vehiculo, ya
/// que dichos objetos 3D son un componente puramente estetico. Si no se utiliza
/// este script, no varia en nada el comportamiento del vehiculo pero las ruedas
/// apareceran visualmente rigidas.
/// </summary>
public class FollowWheel : MonoBehaviour
{
    // Entidad 3D que representa esteticamente a una rueda
    public GameObject WheelModel;

    void Update()
    {
        // La rotacion de la entidad 3D elegida copia a la del 
        // WheelCollider que tiene asignado este script
        Vector3 pos = this.transform.position;
        Quaternion rot = WheelModel.transform.rotation;
        this.GetComponent<WheelCollider>().GetWorldPose(out pos, out rot);
        WheelModel.transform.rotation = rot;
    }
}
