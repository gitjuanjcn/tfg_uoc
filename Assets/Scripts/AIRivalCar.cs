using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

/// <summary>
/// Script para controlar el desplazamiento automatico del jugador en funcion del
/// waypoint actualmente seguido, asi como la velocidad de desplazamiento gobernada
/// por las ruedas implementadas mediante WheelCollider.
/// </summary>
public class AIRivalCar : MonoBehaviour
{
    // Rigidbody del propio vehiculo
    private Rigidbody _carRigidBody;

    // Raiz de los Waypoints a seguir para el desplazamiento automatico
    public GameObject WayPoints;

    // Muestra las vueltas completadas
    public GameObject RivalLapsIcon;

    // Posicion del Waypoint actual a seguir
    private Transform _currentWaypoint;

    // Cantidad de Waypoints e indice del WaypointActual
    private int _waySize, _wayIndex;

    // Angulo de giro maximo que alcanzan las ruedas
    public float maxSteerAngle = 45f;

    // Collider de las ruedas frontales
    public WheelCollider[] wheelsFront = new WheelCollider[2];

    // Velocidad a la que se mueve el vehiculo
    public float Speed;

    // Vueltas que ha completado este vehiculo
    public int LapsCount;

    private void Awake()
    {
        LapsCount = -1;
    }

    void Start()
    {
        // Inicializamos el Rigidbody
        _carRigidBody = transform.parent.GetComponent<Rigidbody>();
        _waySize = WayPoints.GetComponentsInChildren<Transform>().Length - 1;
        _wayIndex = 0;
        _currentWaypoint = WayPoints.transform.GetChild(0);
    }

    private void FixedUpdate()
    {
        // Velocidad del rigidbody
        float velocity = _carRigidBody.velocity.magnitude;
        if (velocity < 10)
            velocity = 10;

        // Mediante InverseTransformPoint obtenemos la posicion del waypoint objetivo 
        Vector3 relVector = transform.InverseTransformPoint(_currentWaypoint.position);

        // Orientamos las ruedas en funcion de la distancia al waypoint
        float steerWheels = (relVector.x / relVector.magnitude) * maxSteerAngle;

        wheelsFront[0].steerAngle = steerWheels;
        wheelsFront[1].steerAngle = steerWheels;

        // Se asigna velocidad y poder de rotacion (torque) a las ruedas
        wheelsFront[0].motorTorque = Speed * 500 * Time.deltaTime;
        wheelsFront[1].motorTorque = Speed * 500 * Time.deltaTime;
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("RivalWayPoints"))
        {
            _wayIndex++;
            
            if (_wayIndex >= _waySize)
                _wayIndex = 0;

            _currentWaypoint = WayPoints.transform.GetChild(_wayIndex);
        }
    }
}
