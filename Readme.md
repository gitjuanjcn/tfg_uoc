<h1>Controles con teclado correspondientes a la version "Build_Gold_ConTeclado"<\h1>


Controles del Menu principal
------------------------------------
Intro -> Carrera por puntos\
Espacio -> Carrera contra rivales\
Escape -> Salir del juego


Controles durante la carrera
------------------------------------
Tecla izquierda -> Girar a la izquierda\
Tecla derecha -> Girar a la derecha\
Espacio -> Pausar


Controles del menu de pausa
------------------------------------
Espacio -> Continuar carrera\
Intro -> Reiniciar carrera\
Escape -> Salir al Menu principal


Controles de fin de carrera
------------------------------------
Intro -> Reiniciar carrera\
Escape -> Salir al Menu principal